-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `mathesio_comments` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mathesio_comments`;

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` tinytext NOT NULL,
  `content` text NOT NULL,
  `added_when` datetime NOT NULL,
  `added_by_user_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `added_by_user_id` (`added_by_user_id`),
  CONSTRAINT `article_ibfk_1` FOREIGN KEY (`added_by_user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `article` (`id`, `title`, `content`, `added_when`, `added_by_user_id`) VALUES
(1,	'Zadání',	'<p>Vytvoř stránku s výpisem článku (staticky, z DB, whatever), který má pod sebou diskuzi, kde mohou diskutující komentovat článek. Jediné omezení je v tom, že komentáře se musí odesílat AJAXem a diskuze (Nette komponenta) se musí obnovit bez reloadu stránky</p>\r\n	<p>Celý princip je ukázat, čím se dokážeš pochlubit. Použiješ nějaký cool framework? Budou se uživatelé nějak přihlašovat, if ano, jak? FB? Google? Lokální seznam uživatelů? Naprosto na tobě. Použiješ na diskuzi klasické jQuery? Angular? React? Vue? Něco vlastního? Opět, naprosto na tobě. To samé platí o backendu, poběží to nad Firebase, MySQL, nebo Mongem? Cokoliv si vymyslíš, pokud dodržíš to kritérium, které je v zadání a budeš hrdý na výsledný kód. Očekávám programátorský samet, pochlub se, ukaž co umíš, předveď se. Bude mě zajímat výsledné řešení, kvalita kódu, komentáře, jak snadno to dokážu rozjet, jak je to navržené z hlediska budoucích modifikací atd.</p>\r\n\r\n	<p>Pokud do toho půjdeš, tak mě zajímají ještě tři čísla:</p>\r\n\r\n	<ul>\r\n		<li>kolik hodin ti to cca zabere: <strong>6 hod</strong></li>\r\n		<li>kdy to bude hotové (datum): <strong>do pátku 12:00</strong></li>\r\n		<li>pokud by to mělo být placené, kolik peněz by sis za takovou věc řekl: <strong>6 x 400 Kč/hod = 2 400 Kč</strong></li>\r\n	</ul>\r\n',	'2018-10-11 08:18:01',	1);

DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(11) unsigned DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `text` text NOT NULL,
  `added_when` datetime NOT NULL,
  `added_by_user_id` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`),
  KEY `deleted` (`deleted`),
  KEY `added_by_user_id` (`added_by_user_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`),
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`added_by_user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` blob NOT NULL,
  `firstname` varchar(128) NOT NULL,
  `surname` varchar(128) NOT NULL,
  `added_when` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `user` (`id`, `email`, `username`, `password`, `firstname`, `surname`, `added_when`, `deleted`) VALUES
(1,	'hermione@seznam.cz',	'Hermione',	'',	'Hermione',	'Granger',	'2018-10-11 08:34:02',	0),
(2,	'harry@seznam.cz',	'Harry',	'',	'Harry',	'Potter',	'2018-10-11 08:37:26',	0);

-- 2018-10-11 17:45:28
