<?php declare(strict_types=1);

namespace App\Presenters;

use App\Controls\ICommentsControlFactory;
use App\Model\Article;

final class HomepagePresenter extends BasePresenter
{
	/** @var int we comment only this article in this demo */
	const ARTICLE_ID = 1;

	/** @var Article @inject */
	public $article;

	/** @var ICommentsControlFactory @inject */
	public $comments_control_factory;



	public function renderDefault()
	{
		$this->template->article = $this->article->getById(self::ARTICLE_ID);
	}



	protected function createComponentComments()
	{
		$control = $this->comments_control_factory->create();
		$control->setArticleId(self::ARTICLE_ID);
		return $control;
	}
}
