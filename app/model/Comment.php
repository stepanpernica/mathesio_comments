<?php declare(strict_types=1);

namespace App\Model;

use Nette\Database\Context;
use Nette\Database\Table\ActiveRow;
use Nette\SmartObject;

final class Comment
{
	use SmartObject;

	const TABLE_NAME = 'comment';

	/** @var Context */
	private $database;



	/**
	 * @param Context $database
	 */
	public function __construct(
		Context $database
	) {
		$this->database = $database;
	}



	/**
	 * @param $article_id
	 * @param $parent_id
	 * @param $text
	 */
	public function add($article_id, $parent_id, $text)
	{
		$this->database->table(self::TABLE_NAME)->insert([
			'article_id' => $article_id,
			'parent_id' => $parent_id,
			'text' => $text,
			'added_when' => new \DateTime(),
		]);
	}



	/**
	 * @param int $id
	 * @return null|false|ActiveRow
	 */
	public function getById(int $id)
	{
		$id = (int) $id;
		if (empty($id)) {
			return null;
		}
		return $this->database->table(self::TABLE_NAME)->where('id=? AND deleted=0', $id)->fetch();
	}



	/**
	 * @param $id
	 * @return array|null
	 */
	public function getCommentsByArticleId($id)
	{
		$id = (int) $id;
		if (empty($id)) {
			return null;
		}

		$comments = $this->database->table(self::TABLE_NAME)->where('article_id=? AND deleted=0', $id)->fetchAll();
		return $this->buildTree($comments);
	}



	/**
	 * @param $items
	 * @return array|null
	 */
	private function buildTree($items)
	{
		$result = [];
		foreach ($items as $item){
			$result[$item->parent_id][] = [
				'id' => $item->id,
				'parent_id' => $item->parent_id,
				'level' => 0,
				'data' => $item,
			];
		}

		if (empty($result)) {
			return NULL;
		}

		return $this->createTree($result, current($result));
	}



	/**
	 * @param $list
	 * @param $parent
	 * @param int $level
	 * @return array
	 */
	private function createTree(&$list, $parent, $level = 0){
		$tree = array();
		foreach ($parent as $k => $item){
			$item['level'] = $level;
			if(isset($list[$item['id']])) {
				$item['children'] = $this->createTree($list, $list[$item['id']], $level + 1 );
			}
			$tree[] = $item;
		}

		return $tree;
	}
}
